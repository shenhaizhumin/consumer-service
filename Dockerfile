FROM python:3.7-slim-stretch

COPY ./requirements.txt /

RUN pip install --no-cache-dir -r ./requirements.txt

COPY . /project

WORKDIR /project

ENTRYPOINT python manage.py
