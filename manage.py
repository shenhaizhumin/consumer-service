# -*- encoding: utf-8 -*-
"""
@Author  : zengqi
@Date    : 2020/11/20
"""
from src.app.transform_chain import *
from src.settings import redis_helper, rabbit_queue_name
import traceback
from src.settings import logger
from src.app.redis_consumer.redis_consumer import RedisConsumer

# from fastapi import FastAPI
from src.settings import sync_db

# app = FastAPI()
#
# flag = True
#
#
# @app.on_event("startup")
# async def startup():
#     print('连接数据库')
#     sync_db.connect()
#     loop()
#
#
# @app.on_event("shutdown")
# async def shutdown():
#     print('关闭数据库')
#     global flag
#     flag = False
#     sync_db.close()


from src.settings import rabbitmq_help
from src.app.rabbit_msg_queue.rabbitmq_consumer import RabbitMqConsumer


# redis 消费者
def start_redis_consumer():
    redis_consumer = RedisConsumer(redis_helper=redis_helper, consuming_function=do_polling,
                                   threads_num=settings.THREAD_NUM)
    redis_consumer.start_consuming_message()


# rabbitmq 消费者
def start_rabbitmq_consumer():
    rabbitmq_consumer = RabbitMqConsumer(rabbitmq_help, rabbit_queue_name, consuming_function=do_polling,
                                         threads_num=settings.THREAD_NUM)
    rabbitmq_consumer.create_channel()
    rabbitmq_consumer.start_consuming_message()


def start():
    logger.info("consumer service start at {}".format(time.time()))
    print("consumer service start at {}".format(time.time()))
    # sync_db.connect()
    try:
        # start_rabbitmq_consumer()
        start_redis_consumer()
    except KeyboardInterrupt as e:
        quit()
        print(e)
    except Exception as e:
        quit()
        print(e)


def quit():
    logger.info("consumer service shutdown at {}".format(time.time()))
    rabbitmq_help.disconnected()
    # sync_db.close()


import sys
import re

if __name__ == '__main__':
    # loop()
    # import uvicorn
    #
    # uvicorn.run(app)
    start()
# sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
# sys.exit(start())
