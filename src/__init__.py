# -*- encoding: utf-8 -*-
"""
@Author  : zengqi
@Date    : 2020/11/22
"""
import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)