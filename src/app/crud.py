#!/usr/bin/python3.6.8+
# -*- coding:utf-8 -*-
"""
@auth: cml
@date: 2020-10-16
@desc: 对Job表的增删改查操作
"""
import json
from src.settings import sync_db
from src.const import JobStatus


class JobCRUD:
    @staticmethod
    async def create_table(db):
        """
        CREATE TABLE job (
          id             BIGSERIAL PRIMARY KEY,
          uid            VARCHAR(40) NOT NULL    UNIQUE,
          type           VARCHAR(40) NOT NULL,
          status         SMALLINT DEFAULT 0,
          message        VARCHAR,
          application_uid VARCHAR NOT NULL,
          data           JSONB,
          start_time      TIMESTAMP WITHOUT TIME ZONE,
          end_time     TIMESTAMP WITHOUT TIME ZONE
        );
        COMMENT ON TABLE job IS '主任务表';
        COMMENT ON COLUMN job.id IS '自增主键ID';
        COMMENT ON COLUMN job.uid IS 'UID';
        COMMENT ON COLUMN job.type IS '任务类型';
        COMMENT ON COLUMN job.status IS '任务状态';
        COMMENT ON COLUMN job.message IS '任务状态简信';
        COMMENT ON COLUMN job.application_id IS '应用id';
        COMMENT ON COLUMN job.data IS '任务关键数据(记录)';
        COMMENT ON COLUMN job.starttime IS '任务开始时间';
        COMMENT ON COLUMN job.finishtime IS '任务结束时间';

        :param db:
        :return:
        """
        sql = """
        CREATE TABLE job (
          id             BIGSERIAL PRIMARY KEY,
          uid            VARCHAR(40) NOT NULL    UNIQUE,
          type           VARCHAR(40) NOT NULL,
          status         SMALLINT DEFAULT 0,
          message        VARCHAR,
          application_uid VARCHAR NOT NULL,
          data           JSONB,
          start_time      TIMESTAMP WITHOUT TIME ZONE,
          end_time     TIMESTAMP WITHOUT TIME ZONE
        );"""
        # query = """CREATE TABLE HighScores (
        # id INTEGER PRIMARY KEY, name VARCHAR(100), score INTEGER)"""
        return await db.execute(query=sql)

    @staticmethod
    async def create(data, db):
        """"""
        # job_uid = '1234abcd'
        sql = """
        INSERT INTO job(uid, type, application_uid, status, start_time) 
        VALUES (:uid, :type, :application_uid, :status, now()); 
        """
        last_record_id = await db.execute(sql, values=data)
        print(last_record_id)
        return last_record_id

    async def bulk_create(self, values: list, db):
        """
        values = [
            {"name": "Daisy", "score": 92},
            {"name": "Neil", "score": 87},
            {"name": "Carol", "score": 43},
        ]

        :param values:
        :return:
        """
        query = """
        INSERT INTO job(uid, type, application_uid, status, start_time) 
        VALUES (:uid, :type, :application_uid, :status, now())"""

        await db.execute_many(query=query, values=values)

    @staticmethod
    async def get(job_uid, db):
        sql = """
        SELECT type, id, status, message, data, start_time 
        FROM job WHERE uid = '{}';
        """.format(job_uid)
        return await db.execute(sql)

    @staticmethod
    async def list(db):
        query = "SELECT * FROM job LIMIT 10;"
        rows = await db.fetch_all(query=query)
        print('High Scores:', rows)
        return rows

    @staticmethod
    async def update(db):
        query = "SELECT * FROM job LIMIT 10;"
        rows = await db.fetch_all(query=query)
        print('High Scores:', rows)
        return rows


def update_job_status(job_uid, status, message='', db_session=None):
    caluse_sql = ''
    if status == JobStatus.PROCESS:
        caluse_sql += ' , start_time = now() '
    elif status == JobStatus.SUCCESS or status == JobStatus.FAILURE:
        caluse_sql += ' , end_time = now() '
    sql = '''
        UPDATE job
        SET status = :status, message = :message
        {caluse_sql}
        WHERE uid = :job_uid;
    '''.format(caluse_sql=caluse_sql)

    args = dict(job_uid=job_uid, status=status, message=message)
    if db_session:
        db_session.execute(sql, args)
    else:
        sync_db.execute(sql, args)


def update_job_data(job_uid, data, db_session=None):
    sql = '''
        UPDATE job
        SET data = '{}'
        WHERE uid = '{}';
    '''.format(
        json.dumps(data), job_uid
    )
    if db_session:
        db_session.execute(sql)
    else:
        sync_db.execute(sql)


def insert_job(job_uid, application_uid, job_type, status=JobStatus.PENDING):
    sql = """
                INSERT INTO job(uid, type, application_uid, status, start_time)
                VALUES ('{0}', '{1}', '{2}', {3}, now())
                RETURNING id AS id, uid AS uid;
            """.format(
        job_uid, job_type, application_uid, status
    )
    sync_db.execute(sql)


'''
# Create a database instance, and connect to it.
from databases import Database
database = Database('sqlite:///example.db')
await database.connect()

# Create a table.
query = """CREATE TABLE HighScores (id INTEGER PRIMARY KEY, name VARCHAR(100), score INTEGER)"""
await database.execute(query=query)

# Insert some data.
query = "INSERT INTO HighScores(name, score) VALUES (:name, :score)"
values = [
    {"name": "Daisy", "score": 92},
    {"name": "Neil", "score": 87},
    {"name": "Carol", "score": 43},
]
await database.execute_many(query=query, values=values)

# Run a database query.
query = "SELECT * FROM HighScores"
rows = await database.fetch_all(query=query)
print('High Scores:', rows)
'''
