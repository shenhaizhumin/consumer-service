# -*- encoding: utf-8 -*-
"""
@Auth: zengqi
@Date: 2020/11/23
"""

import pika


class RabbitMqHelper(object):

    def __init__(self, user, password, host, port, virtual_host, heartbeat):
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.virtual_host = virtual_host
        self.heartbeat = heartbeat
        self.credentials = None
        self.connection = None
        # self.connect()

    def connect(self):
        if not self.connection or self.connection.is_open:
            self.credentials = pika.PlainCredentials(self.user,
                                                     self.password)  # mq用户名和密码# 虚拟队列需要指定参数 virtual_host，如果是默认的可以不填。
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=self.host, port=self.port, virtual_host=self.virtual_host,
                                          credentials=self.credentials,
                                          heartbeat=self.heartbeat))
            print("连接到rabbitmq")

    def disconnected(self):
        if self.connection and not self.connection.is_closed:
            print("close msg queue")
            self.connection.close()

    def create_publish_channel(self, queue_name):
        """创建消息发布通道"""
        channel = self.connection.channel()

        channel.queue_declare(queue=queue_name, durable=True)
        return channel

    def create_consumer_channel(self, queue_name, threads_num):
        """创建消息接收通道"""
        channel = self.create_publish_channel(queue_name)
        channel.basic_qos(prefetch_count=threads_num)
        return channel
