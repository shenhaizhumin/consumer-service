# -*- encoding: utf-8 -*-
"""
@Auth: zengqi
@Date: 2020/11/23
"""
from collections import Callable
import functools
import time
from threading import Lock
from pika import BasicProperties
from concurrent.futures import ThreadPoolExecutor
import traceback
import json
from src.settings import all_task_status_list, RUNNING


class RabbitMqConsumer(object):
    def __init__(self, rabbitmq_help, queue_name, consuming_function: Callable = None, threads_num=100,
                 max_retry_times=3,
                 is_print_detail_exception=True):
        self._queue_name = queue_name
        self.consuming_function = consuming_function
        self.thread_pool = ThreadPoolExecutor(max_workers=threads_num, thread_name_prefix="my_worker_")
        self._max_retry_times = max_retry_times
        self._is_print_detail_exception = is_print_detail_exception
        self.rabbitmq_helper = rabbitmq_help
        self.channel = None
        # self.channel = self.rabbitmq_helper.create_consumer_channel(queue_name, threads_num)

    def create_channel(self):
        self.channel = self.rabbitmq_helper.create_publish_channel(self._queue_name)

    def start_consuming_message(self):
        def callback(ch, method, properties, body):
            msg = body.decode()
            print(f'从rabbitmq取出的消息是：  {msg}')
            # ch.basic_ack(delivery_tag=method.delivery_tag)
            self.thread_pool.submit(self.__consuming_function, ch, method, properties, msg)

        self.channel.basic_consume(on_message_callback=callback,
                                   queue=self._queue_name,
                                   # auto_ack=False  # 如果no_ack=False,当消费者down掉了，RabbitMQ会重新将该任务添加到队列中
                                   )
        self.channel.start_consuming()

    def __consuming_function(self, ch, method, properties, msg, current_retry_times=0):
        print("msg:", msg)
        if current_retry_times < self._max_retry_times:
            # noinspection PyBroadException
            try:
                task_params = json.loads(msg)
                job_uid = task_params.get('job_uid')
                if not job_uid:
                    return
                all_task_status_list.put(job_uid, RUNNING)  # 记录任务状态
                self.consuming_function(task_params.get('data'), job_uid,
                                        task_params.get('job_type'),
                                        task_params.get('task_type'),
                                        task_params.get('task_action'))
                # ch.basic_ack(delivery_tag=method.delivery_tag)
                self.rabbitmq_helper.connection.add_callback_threadsafe(
                    functools.partial(self.ack_message, ch, method.delivery_tag))
            except Exception as e:
                print(f'函数 {self.consuming_function}  第{current_retry_times + 1}次发生错误，\n 原因是{e}')
                self.__consuming_function(ch, method, properties, msg, current_retry_times + 1)
                traceback.print_exc()
        else:
            print(f'达到最大重试次数 {self._max_retry_times} 后,仍然失败')
            # ch.basic_ack(delivery_tag=method.delivery_tag)
            self.rabbitmq_helper.connection.add_callback_threadsafe(
                functools.partial(self.ack_message, ch, method.delivery_tag))

    @staticmethod
    def ack_message(channelx, delivery_tagx):
        """Note that `channel` must be the same pika channel instance via which
        the message being ACKed was retrieved (AMQP protocol constraint).
        """
        if channelx.is_open:
            channelx.basic_ack(delivery_tagx)
        else:
            # Channel is already closed, so we can't ACK this message;
            # log and/or do something that makes sense for your app in this case.
            pass
