# -*- encoding: utf-8 -*-
"""
@Author  : zengqi
@Date    : 2020/11/20
"""
import time
import requests

from src.const import JobStatus
from src.settings import settings
from src.app.crud import update_job_status, update_job_data
from src.core.exceptions import PollingExecutedError
from src.settings import logger, redis_helper, host_cache_name, all_task_status_list, RUNNING, SUCCESS, FAILED, \
    CALLBACK, \
    running_task_list
from requests.exceptions import Timeout
from src.settings import sync_db
from src.core.db.db_session import get_session

redis_connect = redis_helper.connect


def do_polling(data, job_uid, job_type, task_type, task_action):
    print('========>>polling')
    print(data)
    timeout = int(data.get('timeout'))
    session = get_session()
    """轮询任务处理状态"""
    update_job_status(job_uid, JobStatus.POLLING, db_session=session)
    session.commit()
    retries = 0
    task_host = data.get('task_host')
    print("task_host:", task_host)
    # url = settings.REALICLOUD_URL + "/query/{task_id}".format(task_id=job_uid)
    url = task_host + "/query/{task_id}".format(task_id=job_uid)
    while retries <= timeout:
        try:
            resp = requests.get(url, timeout=settings.REQUEST_TIMEOUT)
            print("job:{}".format(resp.text))
            resp_data = resp.json() or dict()
            if resp.status_code == 200:
                # resp_data.get('code')  200 ok
                task_status = resp_data.get('task_status')
                if task_status and int(task_status) == JobStatus.SUCCESS:
                    job_data = resp_data.get('data')
                    update_job_data(job_uid, job_data, db_session=session)
                    session.commit()
                    session.close()
                    # sync_db.close()
                    # 返回处理成功的数据
                    logger.info("polling任务执行成功,job_uid:{}".format(job_uid))
                    print('+++++++++++job polling success++++++++++++++')
                    do_callback(data, data['callback'], job_uid, task_type, task_action)
                    return
                elif task_status == JobStatus.FAILURE:
                    raise Exception('polling 任务执行失败！')
            else:
                raise Exception('请求失败,status code :{}'.format(resp.status_code))
        except Exception as e:
            print('error----------------------执行polling任务出错:{}'.format(e))
            message = "do_polling: 任务执行失败,msg:{}".format(e)
            update_job_status(job_uid, JobStatus.FAILURE, message, session)
            # sync_db.commit()
            session.commit()
            session.close()
            logger.error("ERROR:{}".format(e))
            logger.exception(e)
            data['callback']['error'] = message
            do_callback(data, data['callback'], job_uid, task_type, task_action)
            return
            # raise Exception(e)
        # 重试
        times = 1
        time.sleep(times)
        retries += 1
        print("retry in {} seconds".format(times))
    else:
        # return data
        message = "polling:规定次数内未完成任务"
        update_job_status(job_uid, JobStatus.FAILURE, message, session)
        # sync_db.commit()
        session.commit()
        session.close()
        logger.error("polling任务执行失败,job_uid={},出错信息:{}".format(job_uid, "polling:规定次数内未完成任务,timeout:{}".format(
            timeout)))
        data['callback']['error'] = message
        do_callback(data, data['callback'], job_uid, task_type, task_action)
        # raise MaxRetriesExecuted(message)


def do_callback(data, callback, job_uid, task_type, task_action):
    """回调处理"""
    logger.info('========>>callback')
    print('========>>callback')
    print('job_uid:', job_uid)
    print('callback-----callback:{}'.format(callback))
    callback_url = callback.get('url')
    task_url = data.get('task_url')
    # """任务 -1"""
    # redis_connect.zincrby(host_cache_name, -1, task_url)
    print("task_url:", task_url)
    # handler = handler_factory.get_handler(task_type, task_action)
    # print("handler:{}".format(handler))
    message = callback.get('error') or 'success'
    callback_params = dict(callback=callback, data=data, message=message, task_id=job_uid)
    print("callback_params:{}".format(callback_params))
    retries = 0
    is_success = False
    session = get_session()
    while retries <= settings.MAX_CALLBACK_RETRIES:
        # 发起回调请求
        try:
            resp = requests.post(callback_url, json=callback_params, timeout=settings.REQUEST_TIMEOUT)
            print("callback resp-----", resp)
            if resp.status_code == 200:
                # 回调成功！
                if message == 'success':
                    is_success = True
                    update_job_status(job_uid, JobStatus.SUCCESS, "success", session)
                    # sync_db.commit()
                    session.commit()
                    session.close()
                    job_subduction(job_uid, task_url)  # 任务数量统计 -1
                    # 标记任务执行成功
                    all_task_status_list.put(job_uid, SUCCESS)
                else:
                    job_subduction(job_uid, task_url)  # 任务数量统计 -1
                    # 标记任务执行失败
                    all_task_status_list.put(job_uid, FAILED)

                print('+++++++++++callback success++++++++++++++')
                # 从运行中的任务列表移除
                running_task_list.remove(job_uid)
                return
            else:
                raise Exception('status code:{}'.format(resp.status_code))
        except Timeout:
            print("callback retry in 1 second")
            logger.info("callback超时，job_uid={},超时时间:{}".format(job_uid, settings.REQUEST_TIMEOUT))
            # 回调失败
            time.sleep(get_retry_times(retries))
            retries += 1
        except Exception as e:
            print("callback failed-----", e)
            message = "callback任务执行失败,{}".format(e)
            if not is_success:
                update_job_status(job_uid, JobStatus.CALLBACK, message, session)
                # sync_db.commit()
                session.commit()
                session.close()
            logger.error("callback任务执行失败,job_uid={},出错信息:{}".format(job_uid, message))
            logger.exception(e)
            job_subduction(job_uid, task_url)  # 任务数量统计 -1
            # 标记任务执行失败
            all_task_status_list.put(job_uid, FAILED)
            # 从运行中的任务队列移除
            running_task_list.remove(job_uid)
            return 'callback failed'
    else:
        print("callback：规定次数内未完成任务")
        message = "callback：规定次数内未完成任务"
        update_job_status(job_uid, JobStatus.CALLBACK, message, session)
        # sync_db.commit()
        session.commit()
        session.close()
        logger.error("callback任务执行失败,job_uid={},出错信息:{}".format(job_uid, "callback：规定次数内未完成任务"))
        job_subduction(job_uid, task_url)  # 任务数量统计 -1
        # 标记任务执行失败
        all_task_status_list.put(job_uid, FAILED)
        # 从运行中的任务队列移除
        running_task_list.remove(job_uid)
        # raise MaxRetriesExecuted(message)


def job_subduction(job_uid, task_url):
    value = all_task_status_list.get(job_uid)  # 0 正在运行 1 已成功 -1 已失败
    print("callback 任务状态为:{}".format(value))
    if value and int(value) == RUNNING:  # 防止多消费者重复扣除任务
        """任务 -1"""
        redis_connect.zincrby(host_cache_name, -1, task_url)


def fibonacci(n):  # 生成器函数 - 斐波那契
    a, b, counter = 1, 1, 0
    while True:
        if counter > n:
            return
        yield a
        a, b = b, a + b
        counter += 1


from collections import deque


def get_retry_times(n):
    # return [x for x in fibonacci(n)][-1]
    return deque(fibonacci(n), 1)[0]

# def do_process(data, job_uid, job_type, task_type, task_action):
#     """向底层转换服务发送转换任务"""
#     logger.info("***** do_process start -- job_uid:{}*****".format(job_uid))
#     logger.debug(data)
#     print('=========》进入process')
#     # body = data.get('body')
#     # 处理task_id
#     # body.update(job_uid=job_uid)
#     update_job_status(job_uid, JobStatus.PROCESS)
#     # sync_db.commit()
#     # 发送处理请求
#     task_params = dict(
#         task_id=job_uid,
#         # job_type=job_type,
#         oss_config=data.get('oss_conf'),
#         # data=data.get('data'),
#         file_url=data.get('data').get('file_url')
#     )
#     task_params.update(data)
#     print(f"task_params:{task_params}")
#     # url = settings.REALICLOUD_URL + "/task/{task_type}".format(task_type=job_uid)
#     task_url = data.get('task_url')
#     task_host = data.get('task_host')
#     url = task_host + "/task/{task_type}".format(task_type=job_uid)
#     try:
#         print(url)
#         resp = requests.post(url=url, json=task_params, timeout=settings.request_timeout)
#         if resp.status_code == 200 and resp.json().get('code') == 200:
#             logger.info("process任务执行成功,job_uid:{}".format(job_uid))
#             print('+++++++++++job process success++++++++++++++')
#             do_polling(data, job_uid, task_type, task_action)
#         else:
#             print("--------TaskExecutedError--")
#             raise TaskExecutedError(resp.json())
#     except Exception as e:
#         print("-----------task服务请求异常-----------")
#         import traceback
#         traceback.print_exc()
#         logger.exception(e)
#         message = "do_process: 任务执行失败"
#         update_job_status(job_uid, JobStatus.FAILURE, message)
#         # sync_db.commit()
#         data['callback']['error'] = message
#         do_callback(data['data'], data['callback'], job_uid, task_type, task_action)
#         # raise Exception(e)
