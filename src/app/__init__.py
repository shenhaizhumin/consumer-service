# -*- encoding: utf-8 -*-
"""
@Author  : zengqi
@Date    : 2020/11/22
"""
# import queue
#
# import threading
#
# q = queue.Queue()
#
#
# def producer(i):
#     q.put(i)
#
#
# for x in range(100):
#     t = threading.Thread(target=producer, args=(x,))
#     t.start()
#
# import time
#
#
# def consumer():
#     while True:
#         print(q.get())
#         time.sleep(1)
#
#
# for j in range(3):
#     t = threading.Thread(target=consumer)
#     t.start()
