# -*- encoding: utf-8 -*-
"""
@Auth: zengqi
@Date: 2020/11/24
"""


class RedisSet(object):
    def __init__(self, name, redis_conn):
        self.name = name
        self.conn = redis_conn

    def add(self, value):
        """添加元素"""
        self.conn.sadd(self.name, value)

    def size(self):
        """返回集合大小"""
        return self.conn.scard(self.name)

    def remove(self, value):
        """删除元素value"""
        return self.conn.srem(self.name, value)

    def list(self):
        """返回所有成员"""
        return self.conn.smembers(self.name)
