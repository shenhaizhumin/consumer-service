# -*- encoding: utf-8 -*-
"""
@Auth: zengqi
@Date: 2020/11/24
"""
from collections import Callable
import json
from src.app.redis_consumer.redis_hash import RedisHash
from src.settings import running_task_list, all_task_status_list, RUNNING
from src.settings import rabbit_queue_name, redis_channel_name, logger
from redis.exceptions import RedisError, ConnectionError
import time
import traceback
from concurrent.futures import ThreadPoolExecutor

msg_timeout = 5


class RedisConsumer(object):
    def __init__(self, redis_helper, consuming_function: Callable = None, threads_num=100):
        self.redis_helper = redis_helper
        self.threads_num = threads_num
        self.thread_pool = ThreadPoolExecutor(max_workers=threads_num, thread_name_prefix="my_worker_")
        self.consuming_function = consuming_function

    def handle_message(self, message, from_running_task=False):
        """redis获取到消息后回调该方法"""
        if not from_running_task:
            print(f'从redis channel:{self.redis_helper.channel}取出的消息是：  {message}')
        else:
            print(f'从running_task_list取出的消息是：  {message}')
        task_params = json.loads(message)  # 任务数据
        job_uid = task_params.get('job_uid')
        if not job_uid:
            print("invalid job_uid:{}".format(message))
            return
        self.thread_pool.submit(self.__consuming_function, task_params)
        # 记录运行中的任务数据
        running_task_list.put(job_uid, message)
        all_task_status_list.put(job_uid, RUNNING)

    def __consuming_function(self, task_params: dict):
        """执行任务"""
        # 获取到可靠任务消息并执行
        self.consuming_function(task_params.get('data'), task_params.get('job_uid'),
                                task_params.get('job_type'),
                                task_params.get('task_type'),
                                task_params.get('task_action'))

    def __consuming_incomplete_task(self):
        print("running_task_list count:", running_task_list.size())
        incomplete_tasks = running_task_list.all()
        for message in incomplete_tasks.values():
            self.handle_message(message, True)

    def start_consuming_message(self):
        self.__consuming_incomplete_task()
        # flag = 1
        sub = self.redis_helper.subscribe()  # 订阅消息
        while True:
            # with open('', mode='r') as f:
            #     flag = int(f.read())
            try:
                # 从redis消息队列中接收消息
                if not sub:
                    raise ConnectionError('Connection refused')
                message = sub.get_message(timeout=msg_timeout)
                if not message:
                    continue
                data = message.get('data')
                if not data:
                    continue
                self.handle_message(data)
            except ConnectionError or RedisError:
                print("redis 连接超时正在尝试重连--------")
                time.sleep(1)
                sub = self.redis_helper.subscribe()
                continue
            # except RedisError:
            #     print("超时处理")
            except Exception as e:
                print("error:", e)
                traceback.print_exc()
                logger.exception(e)
                time.sleep(3)
                continue
