# -*- encoding: utf-8 -*-
"""
@Author  : zengqi
@Date    : 2020/11/21
"""


class RedisHash(object):
    def __init__(self, name, redis_conn):
        self.name = name
        self.conn = redis_conn

    def put(self, key, value):
        self.conn.hset(self.name, key, value)

    def remove(self, keys):
        self.conn.hdel(self.name, keys)

    def all(self):
        return self.conn.hgetall(self.name)

    def get(self, key):
        return self.conn.hget(self.name, key)

    def size(self):
        return self.conn.hlen(self.name)
