# -*- encoding: utf-8 -*-
"""
@Auth: zengqi
@Date: 2020/11/24
"""
import redis
import time
from redis.exceptions import RedisError, ConnectionError


class RedisHelper(object):

    def __init__(self, redis_url, channel):
        self.channel = channel
        self.redis_url = redis_url
        self.connect = None
        self.connect_server()

    def connect_server(self):
        self.connect = redis.from_url(self.redis_url, decode_responses=True)

    def ping(self):
        try:
            self.connect.ping()
            print("连接到redis")
            return True
        except RedisError or ConnectionError as e:
            print("ping error:", e)
            self.connect_server()
            return False

    # 发送消息
    def publish(self, msg):
        if self.ping():
            # 发布消息
            self.connect.publish(self.channel, msg)
            return True
        else:
            return False

    # 订阅
    def subscribe(self):
        if self.ping():
            pub = self.connect.pubsub()
            # 订阅通道
            pub.subscribe(self.channel)
            # 准备接收
            pub.parse_response()
            return pub
        else:
            return None
