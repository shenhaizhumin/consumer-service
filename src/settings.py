# -*- encoding: utf-8 -*-
"""
@Author  : zengqi
@Date    : 2020/11/22
"""
import os
import configparser
from pydantic import Field
from src.core.default_settings import DefaultSetting
# from celery.signals import setup_logging
from src.core.logger import get_logger

conf = configparser.ConfigParser()
base_path = os.path.dirname(__file__)
profile = os.environ.get('ENV_PROFILE', 'dev')
print(f"当前环境为：{profile}")
if profile == 'production':
    configname = 'config_production.ini'
elif profile == 'testing':
    configname = 'config_testing.ini'
else:
    configname = 'config.ini'
print(f"===>导入的配置文件为：{configname}")
path = os.path.join(base_path, "conf", configname)
print(path)
conf.read(path)


def get_conf_section_dict(
        section: str,
        exclude: set = None,
        conf_parser: configparser.ConfigParser = conf
):
    """
    获取配置文件某个节选的全部数据，转换成字典

    :param section: 节选名称
    :param exclude: 排除的字段
    :param conf_parser: 配置解析器
    :return:
    """
    conf_dict = dict()
    for k in conf_parser.options(section):
        if exclude and k in exclude:
            break
        conf_dict[k] = conf.get(section, k)
    return conf_dict


class Settings(DefaultSetting):
    PROJECT_NAME: str = "consumer-service"
    DEBUG: bool = True
    API_V1_STR: str = "/api/v1"
    BASE_URL: str = "http://127.0.0.1:8020"

    # postgres数据库的URI
    DB_URI: str = conf.get('dsn', 'transform_uri')

    REALICLOUD_URL: str = conf.get('service_url', 'realicloud')

    # oss_conf: dict = get_conf_section_dict("oss")
    # redis_conf: dict = get_conf_section_dict("redis")
    REDIS_URL: str = get_conf_section_dict("redis").get("url")
    # task_time_limit: dict = get_conf_section_dict("task_time_limit")
    retry_policy: dict = get_conf_section_dict("retry_policy")

    # callback 失败重试次数
    MAX_CALLBACK_RETRIES: int = retry_policy.get('max_callback_retries')
    # 请求超时时间
    REQUEST_TIMEOUT: int = retry_policy.get('request_timeout')
    HOSTS_CACHE_NAME = "my-key"  # 存储 task服务url列表
    # TASK_QUEUE_NAME = 'queue-key'  # 存储等待执行的任务队列
    REDIS_CHANNEL_NAME = 'my-redis-channel'  # redis 订阅消息通道名称
    STATUS_QUEUE_NAME = "status-queue-key"  # 存储所有任务状态 {job_uid:status}
    RUNNING_TASK_QUEUE_NAME = "running-task-queue-name"  # 存储正在运行中的任务数据

    # rabbitmq
    rabbitmq_conf = get_conf_section_dict('rabbitmq')
    RABBITMQ_USER = rabbitmq_conf.get('user')
    RABBITMQ_PASSWORD = rabbitmq_conf.get('password')
    RABBITMQ_PORT = rabbitmq_conf.get('port')
    RABBITMQ_HOST = rabbitmq_conf.get('host')
    RABBITMQ_HEARTBEAT = rabbitmq_conf.get('heartbeat')
    RABBITMQ_VIRTUAL_HOST = rabbitmq_conf.get('virtual_host')
    RABBITMQ_CHANNEL = rabbitmq_conf.get('channel')
    RABBITMQ_QUEUE_NAME = rabbitmq_conf.get('queue_name')

    THREAD_NUM = 20


settings = Settings()

# 建立数据库连接
from src.utils.pg_db import PgDb

sync_db = PgDb(dsn=settings.DB_URI)
logger = get_logger(settings.PROJECT_NAME)

# redis_connect = redis.Redis(decode_responses=True, health_check_interval=30, **settings.redis_conf)
# redis_connect = redis.StrictRedis.from_url(settings.REDIS_URL, decode_responses=True, health_check_interval=30)
from src.app.redis_consumer.redis_helper import RedisHelper
from src.app.redis_consumer.redis_hash import RedisHash

# redis 连接
redis_helper = RedisHelper(settings.REDIS_URL, settings.REDIS_CHANNEL_NAME)

host_cache_name = settings.HOSTS_CACHE_NAME  # 存储 task服务url列表
# task_queue_name = settings.TASK_QUEUE_NAME  # redis存储等待执行的任务队列
redis_channel_name = settings.REDIS_CHANNEL_NAME  # redis 订阅消息通道名称

rabbit_queue_name = settings.RABBITMQ_QUEUE_NAME  # rabbitMq 存储等待执行的任务队列

task_status_queue_name = settings.STATUS_QUEUE_NAME

running_task_queue_name = settings.RUNNING_TASK_QUEUE_NAME  # 存储正在运行中的任务数据

all_task_status_list = RedisHash(task_status_queue_name, redis_helper.connect)  # 提供redis中所有任务的状态查询 {job_uid:status}

running_task_list = RedisHash(running_task_queue_name, redis_helper.connect)  # 正在运行的task {job_uid:task_params}
from src.app.rabbit_msg_queue.rabbitmq_helper import RabbitMqHelper

RUNNING = 0
SUCCESS = 1
FAILED = -1
CALLBACK = 2

# docker构建rabbitmq容器
# docker run -it --rm -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=123456 --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
rabbitmq_help = RabbitMqHelper(user=settings.RABBITMQ_USER, password=settings.RABBITMQ_PASSWORD,
                               host=settings.RABBITMQ_HOST, port=int(settings.RABBITMQ_PORT),
                               virtual_host=settings.RABBITMQ_VIRTUAL_HOST,
                               heartbeat=int(settings.RABBITMQ_HEARTBEAT))
# rabbitmq_help.connect()
