# coding=utf-8

import re
import logging

import psycopg2
from psycopg2.extras import DictCursor


class PgDb(object):

    def __init__(self, dsn=None, **kwargs):
        self.dsn = dsn
        self.kwargs = kwargs
        self.conn = None
        # self.connect()

    def connect(self):
        self.conn = psycopg2.connect(self.dsn, cursor_factory=DictCursor, **self.kwargs)

    @property
    def cur(self):
        if self.conn.closed != 0:
            self.connect()
        return self.conn.cursor()

    def commit(self):
        if self.conn.closed != 0:
            self.connect()
        return self.conn.commit()

    def rollback(self):
        if self.conn.closed != 0:
            self.connect()
        return self.conn.rollback()

    def execute(self, sql, args=None):
        pattern = ":(\w+)([\s|,|;]?)"
        if re.search(pattern, sql):
            sql = re.sub(pattern, "%(\g<1>)s\g<2>", sql)
        logging.debug(sql)
        logging.debug(args)
        cursor = self.cur
        cursor.execute(sql, args)
        return cursor

    def fetchall(self, cursor=None):
        if cursor is not None:
            return cursor.fetchall()
        return self.cur.fetchall()

    def fetchone(self, cursor=None):
        if cursor is not None:
            return cursor.fetchone()
        return self.cur.fetchone()

    def fetchmany(self, size, cursor=None):
        if cursor is not None:
            return cursor.fetchmany(size)
        return self.cur.fetchmany(size)

    @property
    def rowcount(self):
        return self.cur.rowcount

    def close(self):
        self.cur.close()
        self.conn.close()
