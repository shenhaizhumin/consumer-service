#!/usr/bin/python3.7+
# -*- coding:utf-8 -*-
"""
@auth: cml
@date: 2020-10-15
@desc: 常量定义
"""
from enum import Enum


class TaskType(Enum):
    MODEL = 'model'
    CUBEMAP = 'hdr_to_cube'
    TEXTURE = 'texture'
    SCENE = 'scene'
    UI = 'ui'


class TaskAction(Enum):
    CONVERT = 'convert'
    PACKAGE = 'package'
    DOWNLOAD = 'download'
    PUBLISH = 'publish'


class JobType(Enum):
    convert = 1000
    hdr_to_cube = 2000
    # texture = 3000

    pack_model = 4100
    pack_ui = 4200
    pack_rdk = 4300  # sdk打包

    # download = 5000
    publish_scene = 6000


class JobStatus(object):
    PENDING = 0  # 任务等待执行

    STARTED = 100  # 任务执行开始
    PROCESS = 110
    POLLING = 120
    CALLBACK = 130

    SUCCESS = 200  # 任务执行成功
    RETRY = 300  # 任务重试
    FAILURE = 400  # 任务执行失败
    REVOKED = 500  # 任务撤销
