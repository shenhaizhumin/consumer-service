#!/usr/bin/python3.6.8+
# -*- coding:utf-8 -*-
"""
@auth: cml
@date: 2020-8-
@desc: ...
"""
from .default_settings import DefaultSetting


def get_settings():
    try:
        from ..settings import Settings
    except:
        Settings = DefaultSetting

    settings = Settings()
    return settings
